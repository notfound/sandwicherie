Auteurs :

Réalisation :

    WEBER Guillaume
    BARRE David

Conception:

    WEBER Guillaume
    BARRE David
    LATEVE-HOUSSEMAND Gauthier
    RUHLMANN Benjamin

Lien vers Bitbucket :

    https://bitbucket.org/notfound/sandwicherie

Lien vers Webetu :

    https://webetu.iutnc.univ-lorraine.fr/~weber63u/Sandwitch-Framework/PROJET%20SANDWICHERIE/

Description des documents réalisés :

Wireframes :

    - Accueil.html : Détient la page d'accueil et permet la création de sandwich
    - ingrédient.html : Détient une description des ingrédients de la sandwicherie
    - visualisercommande.html : Page qui permet à l'utilisateur de voir le récapitulatif de sa commande
    - modificercommande.html : Page qui permet à l'utilisateur de modifier les différentes commandes que ce dernie réalise

Scénario d'interaction :

    - Scénario.pdf : Détient les descriptifs de pages ainsi que les différents scénarios d'interaction 
